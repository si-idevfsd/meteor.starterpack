/**
 * Stop TypeScript from complaining about `import { Promise } from 'meteor/promise'`
 */

interface MeteorPromiseConstructor extends PromiseConstructor {
      await<T>(arg0: Promise<T>) : T;  // Fibers, man! (Offer not valid in browsers)
}

declare module 'meteor/promise' {
  export var Promise: MeteorPromiseConstructor
}
