import React from 'react'
import { Meteor } from 'meteor/meteor'
import { render } from 'react-dom'
import { App as ReactApp } from '../imports/ui/App'
import Vue from 'vue'
import App from '../imports/ui/App.vue'

Meteor.startup(() => {
  render(<ReactApp/>, document.getElementById('react-target'))
  console.log(App)
  new Vue({
    render: h => h(App),
  }).$mount('#vue-target')
})
